<?php

// Includes
include( get_template_directory() . '/includes/setup.php' );
include( get_template_directory() . '/includes/widgets.php' );
include( get_template_directory() . '/includes/template-tags.php' );
include( get_template_directory() . '/includes/front/enqueue.php' );

include( get_template_directory() . '/includes/theme-customizer.php' );
include( get_template_directory() . '/includes/customizer/sanitize.php' );
include( get_template_directory() . '/includes/customizer/default.php' );
include( get_template_directory() . '/includes/customizer/layout.php' );
include( get_template_directory() . '/includes/customizer/misc.php' );
include( get_template_directory() . '/includes/customizer/social.php' );

//add

include get_template_directory() . '/core/functions/share/custom_share.php';


// Hooks
add_action( 'after_setup_theme', 'simplent_theme_setup' );
add_action( 'widgets_init', 'simplent_widgets_init' );
add_action( 'wp_enqueue_scripts', 'simplent_theme_enqueue' );
add_action( 'customize_register', 'simplent_customize_register' );

//转载请注明来源带原文链接

function add_copyright_text() { ?>
<script type='text/javascript'>
function addLink() {
 	var body_element = document.getElementsByTagName('body')[0];
 	var selection;
 	selection = window.getSelection();
 	var pagelink = "<br /><br /> 转载请注明来源: <a href='"+document.location.href+"'>"+document.location.href+"</a>"; 
 	var copy_text = selection + pagelink;
 	var new_div = document.createElement('div');
 	new_div.style.left='-99999px';
 	new_div.style.position='absolute';
 	body_element.appendChild(new_div );
 	new_div.innerHTML = copy_text ;
 	selection.selectAllChildren(new_div );
 	window.setTimeout(function() {
 		body_element.removeChild(new_div );
 },0);
}
document.oncopy = addLink;
</script>
<?php
}
add_action( 'wp_footer', 'add_copyright_text');



//给文章图片自动添加alt和title信息
add_filter('the_content', 'imagesalt');
function imagesalt($content) {
       global $post;
       $pattern ="/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
       $replacement = '<a$1href=$2$3.$4$5 alt="'.$post->post_title.'" title="'.$post->post_title.'"$6>';
       $content = preg_replace($pattern, $replacement, $content);
       return $content;
}

function image_alt_tag($content){
    global $post;preg_match_all('/<img (.*?)\/>/', $content, $images);
    if(!is_null($images)) {foreach($images[1] as $index => $value)
    {
        $new_img = str_replace('<img', '<img alt="'.get_the_title().' '.get_option('blogname').'"', $images[0][$index]);
        $content = str_replace($images[0][$index], $new_img, $content);}}
    return $content;
}
add_filter('the_content', 'image_alt_tag', 99999);


/* 自动为博客内的连接添加nofollow属性并在新窗口打开链接 */
add_filter('the_content', 'plus_cn_nf_url_parse');

function plus_cn_nf_url_parse($content)
    {
        $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>";
        if (preg_match_all("/$regexp/siU", $content, $matches, PREG_SET_ORDER)) {
            if (!empty($matches)) {
                $srcUrl = get_option('siteurl');
                for ($i = 0; $i < count($matches); $i++) {
                    $tag      = $matches[$i][0];
                    $tag2     = $matches[$i][0];
                    $url      = $matches[$i][0];
                    $noFollow = '';
                    $pattern  = '/target\s*=\s*"\s*_blank\s*"/';
                    preg_match($pattern, $tag2, $match, PREG_OFFSET_CAPTURE);
                    if (count($match) < 1) {
                        $noFollow .= ' target="_blank" ';
                    }
                    $pattern = '/rel\s*=\s*"\s*[n|d]ofollow\s*"/';
                    preg_match($pattern, $tag2, $match, PREG_OFFSET_CAPTURE);
                    if (count($match) < 1) {
                        $noFollow .= ' rel="nofollow" ';
                    }
                    $pos = strpos($url, $srcUrl);
                    if ($pos === false) {
                        $tag = rtrim($tag, '>');
                        $tag .= $noFollow . '>';
                        $content = str_replace($tag2, $tag, $content);
                    }
                }
            }
        }
        $content = str_replace(']]>', ']]>', $content);
        return $content;
    }

/*代码高亮*/

function add_prism() {
        wp_register_style(
            'prismCSS', 
            get_stylesheet_directory_uri() . '/assets/css/prism.css' //自定义路径
         );
          wp_register_script(
            'prismJS',
            get_stylesheet_directory_uri() . '/assets/js/prism.js'   //自定义路径
         );
        wp_enqueue_style('prismCSS');
        wp_enqueue_script('prismJS');
    }
add_action('wp_enqueue_scripts', 'add_prism');

// 禁用 emoji

function disable_emoji()
{
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'disable_emoji_tinymce');
}

add_action('init', 'disable_emoji');


// 替换头像

function sbsb_avatar($avatar)
    {
        $avatar = str_replace(array(
            "www.gravatar.com/avatar/",
            "0.gravatar.com/avatar/",
            "1.gravatar.com/avatar/",
            "2.gravatar.com/avatar/",
            "secure.gravatar.com/avatar/"
        ), "gravatar.loli.net/avatar/", $avatar);
        return $avatar;
    }
add_filter('get_avatar', 'sbsb_avatar', 10, 3);



//替换 JQ
// add_action('init', 'jquery_register');

// function jquery_register()
//     {
//         if (!is_admin()) {
//             wp_deregister_script('jquery');
//             wp_register_script('jquery', ('/wp-content/themes/linmi/assets/js/jquery-3.3.1.min.js'), false, null, true);
//             wp_enqueue_script('jquery');
//         }
//     }


 
?>