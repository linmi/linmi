<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @since Simplent 1.0
 */

get_header();
?>


<div class="two-col-intro">
        <div class="container">
            <div class="grid fourohfour-inner">
                <div class="grid__item one-whole">
                    <p class="text--center">
                        <img src="https://ws2.sinaimg.cn/large/006tNbRwly1fun1h2bde8g30qa0eqmz7.gif" class="fourohfour-gif">
                    </p>
                    <p class="milli text--center">回到首页 <a href="/">linmi.cc</a>.</p>
                </div>
            </div>
        </div>
</div>

<?php get_footer(); ?>