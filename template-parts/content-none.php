<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package WordPress
 * @since Simplent 1.0
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_attr_e( '没有你想要的内容喔', 'simplent' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( esc_attr__( '发表第一篇文章请戳? <a href="%1$s">写作</a>.', 'simplent' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_attr_e( '请试试搜索一下新的关键词吧', 'simplent' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php esc_attr_e( '无法找到你想要的内容，请试试搜索一下新的关键词吧', 'simplent' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
